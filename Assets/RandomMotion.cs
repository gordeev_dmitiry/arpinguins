using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMotion : MonoBehaviour
{
    private float m_Radius = 0.0f;
    private float m_AngleVelocity = 0.0f;
    private Vector3 m_StartPosition;


    private float speed = 1.0f;
    
    // Start is called before the first frame update
    void Start()
    {
        // Assign random radius and angle velocity
        m_StartPosition = transform.position;
        Debug.Log("[Initial object pos] transform.position = " + transform.position + 
                  ", startPosition = " + m_StartPosition);
        
        float minRadius = 0.05f;
        float maxRadius = 0.3f;
        
        System.Random randomRadius = new System.Random();
        m_Radius = (float)randomRadius.NextDouble() * (maxRadius - minRadius) + minRadius;
        Debug.Log("m_Radius = " + m_Radius);

        float minAngleVel = 1.0f;
        float maxAngleVel = 6.0f;
        System.Random randomAngleVel = new System.Random();
        m_AngleVelocity = (float) randomAngleVel.NextDouble() * (maxAngleVel - minAngleVel) + minAngleVel;
        Debug.Log("m_AngleVelocity = " + m_AngleVelocity);
        
        
    }

    // Update is called once per frame
    void Update()
    {
        // // Applying simple rotation around the startPoint
        // float angle = Time.fixedTime * m_AngleVelocity;
        // float xCoord = m_Radius * Mathf.Sin(angle) + m_StartPosition.x;
        // float zCoord = m_Radius * Mathf.Cos(angle) + m_StartPosition.z;
        //
        // transform.position = new Vector3(xCoord, transform.position.y, zCoord); 

        
        
        
        // Debug.Log("[RandomMotion] numObjectsCreated = " + ARCursor.numObjectsCreated);
        // Debug.Log("[RandomMotion] mutualXCoord = " + ARCursor.mutualXCoord);
        // Debug.Log("[RandomMotion] mutualZCoord = " + ARCursor.mutualZCoord);
        
        
        
        
        // Check if the position of the object and target point are close enough.
        if (Vector3.Distance(transform.position, ARCursor.mutualPosition) > 0.1f)
        {
            // Move our position a step closer to the target.
            float step =  speed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, ARCursor.mutualPosition, step);
        }
    }
}

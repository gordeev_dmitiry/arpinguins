using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARCursor : MonoBehaviour
{
    public GameObject cursorChildObject;
    public GameObject objectToPlace;
    public ARRaycastManager raycastManager;

    public bool useCursor = true;

    public static int numObjectsCreated = 0;
    public static Vector3 mutualPosition;
    
    // Start is called before the first frame update
    void Start()
    {
        cursorChildObject.SetActive(useCursor);
    }

    // Update is called once per frame
    void Update()
    {
        if (useCursor)
        {
            UpdateCursor();
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (useCursor)
            {
                Debug.Log("useCursor=" + useCursor);
                Debug.Log("objectToPlace:\n" + objectToPlace);
                Debug.Log("------------------------------------");
                Debug.Log("transform.position:\n" + transform.position);
                Debug.Log("------------------------------------");
                Debug.Log("transform.rotation:\n" + transform.rotation);
                Debug.Log("------------------------------------");
                Instantiate(objectToPlace, transform.position, transform.rotation);

                
                // TODO: vectorize this 
                ARCursor.numObjectsCreated++;
                ARCursor.mutualPosition += transform.position;

                if (numObjectsCreated > 0)
                {
                    ARCursor.mutualPosition /= numObjectsCreated;
                }
                
                Debug.Log("[ARCursor] numObjectsCreated = " + numObjectsCreated);
                Debug.Log("[ARCursor] ARCursor.mutualPosition = " + ARCursor.mutualPosition);
            }
            else
            {
                List<ARRaycastHit> hits = new List<ARRaycastHit>();
                raycastManager.Raycast(Input.GetTouch(0).position, hits, TrackableType.Planes);

                if (hits.Count > 0)
                {
                    Instantiate(objectToPlace, hits[0].pose.position, hits[0].pose.rotation);
                }
            }
        }
    }

    void UpdateCursor()
    {
        Vector2 screenPos = Camera.main.ViewportToScreenPoint(new Vector2(0.5f, 0.5f));
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        raycastManager.Raycast(screenPos, hits, TrackableType.Planes);

        if (hits.Count > 0)
        {
            transform.position = hits[0].pose.position;
            transform.rotation = hits[0].pose.rotation;
        }
    }
}
